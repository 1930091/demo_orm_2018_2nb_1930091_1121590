/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class Automovel implements Serializable {
    
    private String Matricula;
    private Integer Kms;
    private GrupoAutomovel grupo;
    
    public Automovel(String matricula, GrupoAutomovel grupo) {
		this.Matricula = matricula;
		this.grupo = grupo;
    }
    
    public void alteraKms(Integer Kms) {
        this.Kms = Kms;
    }

    public String getMatricula() {
        return Matricula;
    }
    

    public Automovel() {
    }

    
    
    @Override
    public String toString() {
        return "Automovel{" + "Matricula=" + Matricula + ", Kms=" + Kms + '}';
    }

    
    
}
