/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_ORM.util.Console;
import isep.eapli.demo_orm.aplicacao.AutomovelController;
import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.List;

/**
 *
 * @author albertosantos
 */
public class AutomovelUI {
    
    private final AutomovelController controller = new AutomovelController();
    
    public void registarAuto() {

		System.out.println("*** Registo Automóvel ***\n");
		String matricula = Console.readLine("Matricula:");
		System.out.println("*** Grupo Automóvel ***\n");
		List<GrupoAutomovel> listaGA = new GrupoAutomovelController().
			listarGruposAutomoveis();
		if (listaGA != null && listaGA.size() > 0) {
			GrupoAutomovel gA = selecionarGrupoAutomovel(listaGA);
			Automovel automovel = controller.
				registarAutomóvel(matricula, gA);
			System.out.println("Automóvel" + automovel);

		} else {
			System.out.
				println("\n\nNão existe nenhum Grupo Automóvel registado. Comece por registar Grupo Automóvel");
		}
	}

	private GrupoAutomovel selecionarGrupoAutomovel(List<GrupoAutomovel> listaGA) {
		System.out.println("Selecione um dos Grupos de Automóveis");
		for (int i = 0; i < listaGA.size(); i++) {
			System.out.
				println("Opcao:" + String.valueOf(i + 1) + " ......" + listaGA.
					get(i));
		}
		int opcao = Console.readInteger(
			"Qual o nº do Grupo Automóvel a seleccionar?");
		return listaGA.get(opcao - 1);

	}

	public void listarAutos() {
		List<Automovel> lista = controller.listarAutomoveis();
		if (lista == null || lista.isEmpty()) {
			System.out.println("Não existe nenhum automóvel");
		} else {
			for (Automovel result : lista) {
				System.out.println(result);
			}
		}
	}

	public void procurarAutoPorID(String matricula) {
		Automovel auto = controller.procurarAutomovel(matricula);
		if (auto != null) {
			System.out.println("Automovel:" + auto);
		} else {
			System.out.println("Não existe");
		}

	}
}
