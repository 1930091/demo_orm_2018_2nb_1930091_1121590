/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_ORM.util.Console;
import java.util.List;

/**
 *
 * @author mcn
 */
public class GrupoAutomovelUI {

    private final GrupoAutomovelController controller = new GrupoAutomovelController();

    public void registarGA() {
        System.out.println("*** Registo Grupo Automóvel ***\n");
        String nome = Console.readLine("Nome:");
        int portas = Console.readInteger("Número de portas");
        String classe = Console.readLine("Classe:");
        GrupoAutomovel grupoAutomovel = controller.
                registarGrupoAutomóvel(nome, portas, classe);
        System.out.println("Grupo Automóvel" + grupoAutomovel);
    }

    public void listarGAs() {

        List<GrupoAutomovel> listaGA = controller.listarGruposAutomoveis();
        for (GrupoAutomovel result : listaGA) {
            System.out.println(result);

        }

    }

    public void procurarGAPorID(int id) {
        GrupoAutomovel grupoAut = controller.procurarGrupoAutomovel(id);

        if (grupoAut != null) {
            System.out.println("grupo automovel : " + grupoAut.toString());
        } else {

            System.out.println("não existe grupo automovel");

        }

    }

}
