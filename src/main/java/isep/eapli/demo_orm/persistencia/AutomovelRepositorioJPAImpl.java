/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author albertosantos
 */
public class AutomovelRepositorioJPAImpl extends  JpaRepository<Automovel, Long> implements AutomovelRepositorio  {
  
    
    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("DEMO_ORMPU");
        EntityManager manager = factory.createEntityManager();
        return manager;
    }
@Override
    public Automovel add(Automovel automovel) {
        if (automovel == null) {
            throw new IllegalArgumentException();
        }
        EntityManager em = getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(automovel);
        tx.commit();
        em.close();

        return automovel;
    }

    
    /**
     * reads an entity GrupoAutomovel given its ID
     *
     * @param id
     * @return
     */
    @Override
    public Automovel findById(Long id) {
        return getEntityManager().find(Automovel.class, id);
    }

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    //@SuppressWarnings("unchecked")
    @Override
    public List<Automovel> findAll() {
        Query query = getEntityManager().createQuery(
                "SELECT e FROM Automovel e");
        List<Automovel> list = query.getResultList();
        return list;
    }
    
}
