/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;

import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_orm.persistencia.AutomovelRepositorio;
import isep.eapli.demo_orm.persistencia.AutomovelRepositorioJPAImpl;
import java.util.List;

/**
 *
 * @author albertosantos
 */
public class AutomovelController {
    
    public Automovel registarAutomóvel(String matricula, GrupoAutomovel gAuto) {

		
        Automovel auto = new Automovel(matricula, gAuto);
		AutomovelRepositorio repo = new AutomovelRepositorioJPAImpl();
		return repo.add(auto);
	}

	public List<Automovel> listarAutomoveis() {
		AutomovelRepositorio repo = new AutomovelRepositorioJPAImpl();
		return repo.findAll();
	}

	public Automovel procurarAutomovel(String matricula) {
		AutomovelRepositorio repo = new AutomovelRepositorioJPAImpl();
		return repo.findById(matricula);
	}
}
